﻿using System;

namespace BusPrediction
{
    public interface IApplicationCloser
    {
        void CloseApplication();
    }
}
