﻿using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using BusPrediction.Models;

namespace BusPrediction.ViewModels
{
    public class StopsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = null) =>
           PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private App app;

        private List<Stop> allStops;
        public ObservableCollection<Stop> Stops { get; set; }

        public Command BackCommand { get; set; }
        public Command HomeCommand { get; set; }

        public StopsViewModel(App app, List<Stop> stops)
        {
            this.app = app;
            allStops = new List<Stop>(stops);
            Stops = new ObservableCollection<Stop>(stops);

            BackCommand = new Command(async () => await app.GoBack());
            HomeCommand = new Command(async () => await app.GoHome());
        }

        public void FilterStops(string filter)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var lowerFilter = filter.ToLowerInvariant();
                while (Stops.Count > 0)
                    Stops.RemoveAt(0);
                foreach (var stop in allStops)
                {
                    var lowerStopDescription = (stop.Descricao ?? "").ToLowerInvariant();
                    var lowerStopAddress = (stop.Endereco ?? "").ToLowerInvariant();
                    if (lowerStopDescription.Contains(lowerFilter) || lowerStopAddress.Contains(lowerFilter) || string.IsNullOrEmpty(lowerFilter))
                        Stops.Add(stop);
                }
            });
        }
    }
}
