﻿
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using BusPrediction.Models;
using BusPrediction.Views;

namespace BusPrediction.ViewModels
{
    public class DirectionsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = null) =>
           PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private App app;

        private Direction firstDirection;
        public Direction FirstDirection { get { return firstDirection; } set { firstDirection = value; OnPropertyChanged(); } }

        private Direction secondDirection;
        public Direction SecondDirection { get { return secondDirection; } set { secondDirection = value; OnPropertyChanged(); } }

        public Command BackCommand { get; set; }
        public Command FirstDirectionCommand { get; set; }
        public Command SecondDirectionCommand { get; set; }

        public DirectionsViewModel(App app, List<Direction> directions)
        {
            this.app = app;
            if (directions.Count > 0)
                FirstDirection = directions[0];
            if (directions.Count > 1)
                SecondDirection = directions[1];

            BackCommand = new Command(async () => await app.CloseModal());
            FirstDirectionCommand = new Command(async () => await GoToStopsPage(0));
            SecondDirectionCommand = new Command(async () => await GoToStopsPage(1));
        }

        private async Task GoToStopsPage(int i)
        {
            var selectedDirection = i == 0 ? FirstDirection : SecondDirection;
            app.CloseModal();
            app.GoToPage(new StopsPage(app, selectedDirection.LsPontoInteresse));
        }
    }
}
