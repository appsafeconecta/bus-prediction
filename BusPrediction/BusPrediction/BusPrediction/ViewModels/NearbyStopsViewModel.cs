﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using BusPrediction.Models;
using BusPrediction.Services;
using BusPrediction.Models.Location;

namespace BusPrediction.ViewModels
{
    public class NearbyStopsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = null) =>
           PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private App app;

        private bool busy;
        public bool IsBusy { get { return busy; } set { busy = value; OnPropertyChanged(); } }

        private bool waitingForLocation;
        public bool IsWaitingForLocation { get { return waitingForLocation; } set { waitingForLocation = value; OnPropertyChanged(); } }

        private bool showNoStopsText;
        public bool ShowNoStopsText { get { return showNoStopsText; } set { showNoStopsText = value; OnPropertyChanged(); } }

        private int distanceToShow;
        public int DistanceToShow { get { return distanceToShow; } set { distanceToShow = value; OnPropertyChanged(); } }

        private bool radiusIsHigherThanMinimum;
        public bool RadiusIsHigherThanMinimum { get { return radiusIsHigherThanMinimum; } set { radiusIsHigherThanMinimum = value; OnPropertyChanged(); } }

        private bool radiusIsLowerThanMaximum;
        public bool RadiusIsLowerThanMaximum { get { return radiusIsLowerThanMaximum; } set { radiusIsLowerThanMaximum = value; OnPropertyChanged(); } }

        private GeographicLocation userLocation;

        private List<StopGroup> allStops;
        public ObservableCollection<StopGroup> Stops { get; set; }

        private string filter;

        public Command BackCommand { get; set; }
        public Command HomeCommand { get; set; }
        public Command UpdateCommand { get; set; }
        public Command DecrementRadiusCommand { get; set; }
        public Command IncrementRadiusCommand { get; set; }

        public NearbyStopsViewModel(App app)
        {
            this.app = app;
            allStops = new List<StopGroup>();
            Stops = new ObservableCollection<StopGroup>();

            filter = "";

            BackCommand = new Command(async () => await app.GoBack());
            HomeCommand = new Command(async () => await app.GoHome());
            UpdateCommand = new Command(() => UpdateUserLocation());
            DecrementRadiusCommand = new Command(() => DecrementRadius());
            IncrementRadiusCommand = new Command(() => IncrementRadius());

            DistanceToShow = Application.Current.Properties.ContainsKey("Radius") ? (int)Application.Current.Properties["Radius"] : 2;
            RadiusIsHigherThanMinimum = true;
            RadiusIsLowerThanMaximum = true;
        }

        private async Task GetStopsFromService(string companyUrl)
        {
            Exception error = null;
            try
            {
                IsBusy = true;
                var routesService = new RoutesService();
                var items = await routesService.GetRoutes(companyUrl);
                Device.BeginInvokeOnMainThread(() =>
                {
                    while (Stops.Count > 0)
                        Stops.RemoveAt(0);
                    allStops.Clear();
                    app.Routes.Clear();
                    foreach (var route in items)
                    {
                        foreach (var direction in route.LsRoteiro)
                        {
                            List<Stop> directionStops = new List<Stop>();
                            foreach (var stop in direction.LsPontoInteresse)
                            {
                                stop.DistanceFromUser = Formulas.GetKilometersBetween(userLocation.Latitude, userLocation.Longitude, stop.LatitudeCkpt, stop.LongitudeCkpt) * 1000;
                                directionStops.Add(stop);
                            }
                            if (directionStops.Count > 0)
                            {
                                string groupTitle = route.LinhaDescricao + " (" + direction.Roteiro + ")";
                                StopGroup stopGroup = new StopGroup(groupTitle, directionStops.OrderBy(o => o.DistanceFromUser));
                                allStops.Add(stopGroup);
                            }
                        }
                        app.Routes.Add(route);
                    }
                    app.Routes.LastUpdate = DateTime.UtcNow;
                });
            }
            catch (Exception ex)
            {
                error = ex;
            }
            finally
            {
                IsBusy = false;
                FilterStops();
            }
            if (error != null)
            {
                await app.MainPage.DisplayAlert("Erro", "Não foi possível retornar os pontos próximos. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                await app.GoBack();
            }
            return;
        }

        private void GetStopsFromApp()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                while (Stops.Count > 0)
                    Stops.RemoveAt(0);
                allStops.Clear();
                foreach (var route in app.Routes)
                {
                    foreach (var direction in route.LsRoteiro)
                    {
                        List<Stop> directionStops = new List<Stop>();
                        foreach (var stop in direction.LsPontoInteresse)
                        {
                            stop.DistanceFromUser = Formulas.GetKilometersBetween(userLocation.Latitude, userLocation.Longitude, stop.LatitudeCkpt, stop.LongitudeCkpt) * 1000;
                            directionStops.Add(stop);
                        }
                        if (directionStops.Count > 0)
                        {
                            string groupTitle = route.LinhaDescricao + " (" + direction.Roteiro + ")";
                            StopGroup stopGroup = new StopGroup(groupTitle, directionStops.OrderBy(o => o.DistanceFromUser));
                            allStops.Add(stopGroup);
                        }
                    }
                }
            });
            FilterStops();
        }

        /*
        private double GetDistanceFromUser(Stop stop)
        {
            double degreesToRad = Math.PI / 180f;
            double lat1 = userLocation.Latitude;
            double lng1 = userLocation.Longitude;
            double lat2 = stop.LatitudeCkpt;
            double lng2 = stop.LongitudeCkpt;
            double R = 6371;
            double dLat = (lat2 - lat1) * degreesToRad;
            double dLon = (lng2 - lng1) * degreesToRad;
            double lat1rad = lat1 * degreesToRad;
            double lat2rad = lat2 * degreesToRad;

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1rad) * Math.Cos(lat2rad);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return R * c;
        }
        */

        public void ChangeFilter(string newFilter)
        {
            filter = newFilter;
            FilterStops();
        }

        private void FilterStops()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var lowerFilter = filter.ToLowerInvariant();
                while (Stops.Count > 0)
                    Stops.RemoveAt(0);
                foreach (var allGroup in allStops)
                {
                    List<Stop> directionStops = new List<Stop>();
                    foreach (var stop in allGroup)
                    {
                        bool stringFiltered = false;
                        var lowerStopDescription = (stop.Descricao ?? "").ToLowerInvariant();
                        var lowerStopAddress = (stop.Endereco ?? "").ToLowerInvariant();
                        var lowerTitle = allGroup.Title.ToLowerInvariant();
                        if (lowerStopDescription.Contains(lowerFilter) ||
                            lowerStopAddress.Contains(lowerFilter) ||
                            lowerTitle.Contains(lowerFilter) ||
                            string.IsNullOrEmpty(lowerFilter))
                            stringFiltered = true;
                        bool radiusFiltered = stop.DistanceFromUser / 1000 <= DistanceToShow;
                        if (stringFiltered && radiusFiltered)
                            directionStops.Add(stop);
                    }
                    if (directionStops.Count > 0)
                    {
                        StopGroup stopGroup = new StopGroup(allGroup.Title, directionStops.OrderBy(o => o.DistanceFromUser));
                        Stops.Add(stopGroup);
                    }
                }

                ShowNoStopsText = Stops.Count <= 0;
            });
        }

        public void UpdateUserLocation()
        {
            IsBusy = true;
            IsWaitingForLocation = true;
            ShowNoStopsText = false;
            var locationTracker = DependencyService.Get<ILocationTracker>();
            locationTracker.LocationChanged += OnLocationTrackerLocationChanged;
            locationTracker.StartTracking();
        }

        async void OnLocationTrackerLocationChanged(object sender, GeographicLocation args)
        {
            System.Diagnostics.Debug.WriteLine(args.Latitude.ToString("0.00") + ", " + args.Longitude.ToString("0.00"));

            var locationTracker = DependencyService.Get<ILocationTracker>();
            locationTracker.PauseTracking();
            IsWaitingForLocation = false;

            userLocation = args;

            if (app.Routes.Count > 0)
                GetStopsFromApp();
            else
                await GetStopsFromService(Constants.Url);

            FilterStops();

            IsBusy = false;
        }

        private void DecrementRadius()
        {
            DistanceToShow = Math.Max(DistanceToShow - 1, 1);
            Application.Current.Properties["Radius"] = DistanceToShow;
            RadiusIsHigherThanMinimum = DistanceToShow > 1;
            RadiusIsLowerThanMaximum = DistanceToShow < 100;
            FilterStops();
        }

        private void IncrementRadius()
        {
            DistanceToShow = Math.Min(DistanceToShow + 1, 100);
            Application.Current.Properties["Radius"] = DistanceToShow;
            RadiusIsHigherThanMinimum = DistanceToShow > 1;
            RadiusIsLowerThanMaximum = DistanceToShow < 100;
            FilterStops();
        }

        public void UnregisterOnLocationChangedEvent()
        {
            var locationTracker = DependencyService.Get<ILocationTracker>();
            locationTracker.LocationChanged -= OnLocationTrackerLocationChanged;
            locationTracker.PauseTracking();
        }
    }
}
