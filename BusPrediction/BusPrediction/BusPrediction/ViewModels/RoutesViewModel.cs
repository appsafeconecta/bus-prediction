﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using BusPrediction.Models;
using BusPrediction.Services;

namespace BusPrediction.ViewModels
{
    public class RoutesViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = null) =>
           PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private App app;

        private bool busy;
        public bool IsBusy { get { return busy; } set { busy = value; OnPropertyChanged(); } }

        private List<Route> allRoutes;
        public ObservableCollection<Route> Routes { get; set; }

        public Command BackCommand { get; set; }
        public Command HomeCommand { get; set; }

        public RoutesViewModel(App app)
        {
            this.app = app;
            allRoutes = new List<Route>();
            Routes = new ObservableCollection<Route>();

            BackCommand = new Command(async () => await app.GoBack());
            HomeCommand = new Command(async () => await app.GoHome());
        }

        /*
        public async Task GetRoutesFromService(string companyUrl)
        {
            Exception error = null;
            try
            {
                IsBusy = true;
                var routesService = new RoutesService();
                var items = await routesService.GetRoutes(companyUrl);
                Device.BeginInvokeOnMainThread(() =>
                {
                    allRoutes.Clear();
                    while (Routes.Count > 0)
                        Routes.RemoveAt(0);
                    app.Routes.Clear();
                    foreach (var route in items)
                    {
                        Routes.Add(route);
                        allRoutes.Add(route);
                        app.Routes.Add(route);
                    }
                    app.Routes.LastUpdate = DateTime.UtcNow;
                });
            }
            catch (Exception ex)
            {
                error = ex;
            }
            finally
            {
                IsBusy = false;
            }
            if (error != null)
            {
                await app.MainPage.DisplayAlert("Erro", "Não foi possível retornar as linhas. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                await app.GoBack();
            }
            return;
        }
        */

        public void GetRoutesFromApp()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                allRoutes.Clear();
                while (Routes.Count > 0)
                    Routes.RemoveAt(0);
                foreach (var route in app.Routes)
                {
                    Routes.Add(route);
                    allRoutes.Add(route);
                }
            });
        }

        public void FilterRoutes(string filter)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var lowerFilter = filter.ToLowerInvariant();
                while (Routes.Count > 0)
                    Routes.RemoveAt(0);
                foreach (var route in allRoutes)
                {
                    var lowerRouteTitle = route.LinhaDescricao.ToLowerInvariant();
                    if (lowerRouteTitle.Contains(lowerFilter) || string.IsNullOrEmpty(lowerFilter))
                        Routes.Add(route);
                }
            });
        }

        public async Task<bool> UpdateRoutes()
        {
            IsBusy = true;
            bool success = await app.UpdateRoutes();
            IsBusy = false;
            return success;
        }
    }
}
