﻿using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using BusPrediction.Views;
using BusPrediction.Models.Location;

namespace BusPrediction.ViewModels
{
    public class MainMenuViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = null) =>
           PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private App app;

        private bool busy;
        public bool IsBusy { get { return busy; } set { busy = value; OnPropertyChanged(); } }

        private ILocationPermissionVerifier locationPermissionVerifier;
        private ILocationPermissionRequester locationPermissionRequester;

        public Command ContactCommand { get; set; }
        public Command NearbyStopsCommand { get; set; }

        public MainMenuViewModel(App app)
        {
            this.app = app;

            ContactCommand = new Command(async () => { await app.GoToPage(new ContactPage(app)); });
            NearbyStopsCommand = new Command(async () => { await GoToRoutesPage(); });

            locationPermissionVerifier = DependencyService.Get<ILocationPermissionVerifier>();
            locationPermissionRequester = DependencyService.Get<ILocationPermissionRequester>();
            locationPermissionRequester.RequestLocationPermissionResult += OnRequestLocationPermissionResult;
        }

        private async Task GoToRoutesPage()
        {
            var result = await app.MainPage.DisplayAlert("Atenção", "Deseja que a sua localização seja utilizada para mostrar os pontos próximos a você?", "Sim", "Não");
            if (result)
            {
                if (locationPermissionVerifier.IsPermissionGranted())
                {
                    await app.GoToPage(new NearbyStopsPage(app));
                }
                else
                {
                    locationPermissionRequester.RequestLocationPermission();
                }
            }
            else
            {
                await app.MainPage.DisplayAlert("Aviso", "Serão mostrados todos os pontos.", "OK");
                await app.GoToPage(new RoutesPage(app));
            }
        }

        async void OnRequestLocationPermissionResult(object sender, bool args)
        {
            if (args)
            {
                await app.GoToPage(new NearbyStopsPage(app));
            }
            else
            {
                await app.MainPage.DisplayAlert("Aviso", "A permissão de Localização não foi liberada para esse aplicativo. Por favor, permita o acesso à sua localização quando requisitado. Enquanto isso serão mostrados todos os pontos.", "OK");
                await app.GoToPage(new RoutesPage(app));
            }
        }
    }
}
