﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using BusPrediction.Models;
using BusPrediction.Models.Transoft;
using BusPrediction.Services;
using BusPrediction.Views;

namespace BusPrediction.ViewModels
{
    public class BusesViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = null) =>
           PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private App app;

        private bool busy;
        public bool IsBusy { get { return busy; } set { busy = value; OnPropertyChanged(); } }

        private string route;
        public string Route { get { return route; } set { route = value; OnPropertyChanged(); } }

        private string stop;
        public string Stop { get { return stop; } set { stop = value; OnPropertyChanged(); } }

        private string lastUpdate;
        public string LastUpdate { get { return lastUpdate; } set { lastUpdate = value; OnPropertyChanged(); } }

        private bool showNoBusesText;
        public bool ShowNoBusesText { get { return showNoBusesText; } set { showNoBusesText = value; OnPropertyChanged(); } }

        private Stop selectedStop;

        public ObservableCollection<Bus> Buses { get; set; }

        public Command BackCommand { get; set; }
        public Command HomeCommand { get; set; }
        public Command UpdateCommand { get; set; }

        public BusesViewModel(App app, Stop stop)
        {
            this.app = app;
            selectedStop = stop;
            Buses = new ObservableCollection<Bus>();

            BackCommand = new Command(async () => await app.GoBack());
            HomeCommand = new Command(async () => await app.GoHome());
            UpdateCommand = new Command(async () => await UpdateBuses());
        }

        public async Task UpdateBuses()
        {
            IsBusy = true;
            ShowNoBusesText = false;

            await app.UpdateRoutes();

            selectedStop = GetStopFromRoutes(app.Routes);
            if (selectedStop != null)
            {
                int temp = Buses.Count;
                foreach (var bus in selectedStop.LsVeiculo)
                    Buses.Add(bus);
                for (int i = 0; i < temp; i++)
                    Buses.RemoveAt(0);

                Route = selectedStop.Direction.Route.LinhaDescricao;
                Stop = selectedStop.Descricao + " " + selectedStop.Endereco;
                LastUpdate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                if (Buses.Count == 0)
                    ShowNoBusesText = true;
            }
            else
            {
                await app.MainPage.DisplayAlert("Erro", "Não há mais informações desse ponto.", "OK");
                await app.GoHome();
            }

            IsBusy = false;
        }

        private Stop GetStopFromRoutes(List<Route> routes)
        {
            Stop updatedStop = null;
            foreach (var route in routes)
                if (route.LinhaCodigo == selectedStop.Direction.Route.LinhaCodigo)
                    foreach (var direction in route.LsRoteiro)
                        if (object.Equals(direction.Roteiro, selectedStop.Direction.Roteiro) && direction.TipoRoteiro == selectedStop.Direction.TipoRoteiro)
                            foreach (var stop in direction.LsPontoInteresse)
                                if (stop.IdPontoInteresse == selectedStop.IdPontoInteresse)
                                    updatedStop = stop;
            return updatedStop;
        }

        public async Task OpenMap(Bus selectedBus)
        {
            Exception error = null;
            RoutePoints routePoints = null;
            try
            {
                IsBusy = true;
                int routeId = selectedStop.Direction.Route.LinhaCodigo;
                routePoints = app.GetRoutePoints(routeId);
                if (routePoints == null)
                {
                    var routePointsService = new RoutePointsService();
                    var items = await routePointsService.GetRoutePoints(routeId);
                    routePoints = new RoutePoints(routeId);
                    foreach (var routePoint in items)
                    {
                        routePoints.Add(routePoint);
                    }
                    app.RoutePointsList.Add(routePoints);
                }
            }
            catch (Exception ex)
            {
                error = ex;
            }
            finally
            {
                IsBusy = false;
            }
            if (error != null)
            {
                await app.MainPage.DisplayAlert("Erro", "Não foi possível retornar o trajeto do ônibus escolhido. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
            }
            else
            {
                await app.OpenModal(new MapPage(app, selectedBus, selectedStop.Direction.LsPontoInteresse, routePoints));
            }
        }
    }
}
