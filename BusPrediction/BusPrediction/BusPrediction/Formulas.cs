﻿using System;

namespace BusPrediction
{
    public static class Formulas
    {
        public static double GetKilometersBetween(double lat1, double lng1, double lat2, double lng2)
        {
            double degreesToRad = Math.PI / 180f;
            double R = 6371;
            double dLat = (lat2 - lat1) * degreesToRad;
            double dLon = (lng2 - lng1) * degreesToRad;
            double lat1rad = lat1 * degreesToRad;
            double lat2rad = lat2 * degreesToRad;

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1rad) * Math.Cos(lat2rad);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return R * c;
        }
    }
}
