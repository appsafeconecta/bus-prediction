﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BusPrediction.Models;
using BusPrediction.ViewModels;

namespace BusPrediction.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RoutesPage : ContentPage
    {
        private App app;
        private RoutesViewModel routesViewModel;

        public RoutesPage(App app)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            this.app = app;
            routesViewModel = new RoutesViewModel(app);
            BindingContext = routesViewModel;
            ListViewRoutes.ItemSelected += ListViewRoutes_ItemSelected;
        }

        private async void ListViewRoutes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedRoute = e.SelectedItem as Route;
            if (selectedRoute != null)
            {
                ListViewRoutes.SelectedItem = null;
                await app.OpenModal(new DirectionsPage(app, selectedRoute.LsRoteiro));
            }
        }

        private void SearchBarRoute_TextChanged(object sender, TextChangedEventArgs e)
        {
            routesViewModel.FilterRoutes(e.NewTextValue);
        }

        protected override bool OnBackButtonPressed()
        {
            app.GoBack();
            return true;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            bool success = await routesViewModel.UpdateRoutes();
            if (success)
                routesViewModel.GetRoutesFromApp();
            else
                await app.GoBack();
        }
    }
}
