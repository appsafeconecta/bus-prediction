﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BusPrediction.Models;
using BusPrediction.ViewModels;

namespace BusPrediction.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NearbyStopsPage : ContentPage
    {
        private App app;
        private NearbyStopsViewModel nearbyStopsViewModel;

        public NearbyStopsPage(App app)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            this.app = app;
            nearbyStopsViewModel = new NearbyStopsViewModel(app);
            BindingContext = nearbyStopsViewModel;
            ListViewStops.ItemSelected += ListViewStops_ItemSelected;
            LoadingIsBusy.GestureRecognizers.Add(new TapGestureRecognizer());
        }

        private async void ListViewStops_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedStop = e.SelectedItem as Stop;
            if (selectedStop != null)
            {
                ListViewStops.SelectedItem = null;
                await app.GoToPage(new BusesPage(app, selectedStop));
            }
        }

        private void SearchBarStop_TextChanged(object sender, TextChangedEventArgs e)
        {
            nearbyStopsViewModel.ChangeFilter(e.NewTextValue.ToLowerInvariant());
        }

        protected override bool OnBackButtonPressed()
        {
            app.GoBack();
            return true;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            nearbyStopsViewModel.UpdateUserLocation();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            nearbyStopsViewModel.UnregisterOnLocationChangedEvent();
        }
    }
}
