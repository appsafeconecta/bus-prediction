﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BusPrediction.Models.Mail;

namespace BusPrediction.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactPage : ContentPage
    {
        private App app;

        public ContactPage(App app)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            this.app = app;
            ButtonSend.Clicked += ButtonSend_Clicked;
            EditorMessage.Keyboard = Keyboard.Create(KeyboardFlags.CapitalizeSentence);
            EntryName.Keyboard = Keyboard.Create(KeyboardFlags.CapitalizeSentence);
            LoadingSend.GestureRecognizers.Add(new TapGestureRecognizer());
            PickerRoute.Items.Clear();
            foreach (var route in app.Routes)
            {
                PickerRoute.Items.Add(route.LinhaDescricao);
            }
            TimePicker.Time = DateTime.Now.TimeOfDay;
        }

        private async void ButtonSend_Clicked(object sender, EventArgs e)
        {
            string name = EntryName.IsVisible ? EntryName.Text : "";
            string email = EntryEmail.IsVisible ? EntryEmail.Text : "";
            string telephone = EntryTelephone.IsVisible ? EntryTelephone.Text : "";
            bool wantsResponse = SwitchResponse.IsVisible ? SwitchResponse.IsToggled : false;
            string bus = EntryBus.Text ?? "";
            int selectedRouteIndex = PickerRoute.SelectedIndex;
            string route = selectedRouteIndex != -1 ? PickerRoute.Items[selectedRouteIndex] : "";
            DateTime date = DatePicker.Date + TimePicker.Time;
            string message = EditorMessage.Text ?? "";
            if (PickerType.SelectedIndex > -1 && PickerSubject.SelectedIndex > -1)
            {
                string type = PickerType.Items[PickerType.SelectedIndex];
                string subject = PickerSubject.Items[PickerSubject.SelectedIndex];
                var result = await app.MainPage.DisplayAlert("Enviar", "Deseja enviar a ocorrência?", "Sim", "Cancelar");
                if (result)
                {
                    // TODO chamada para WebService
                    LoadingSend.IsVisible = true;
                    Exception error = null;
                    try
                    {
                        IMailSender mailSender = DependencyService.Get<IMailSender>();
                        await mailSender.Send(name, email, type, subject, message, bus, route, telephone, wantsResponse, date);
                    }
                    catch (Exception ex)
                    {
                        error = ex;
                    }
                    finally
                    {
                        LoadingSend.IsVisible = false;
                        if (error != null)
                        {
                            await app.MainPage.DisplayAlert("Erro", "Não foi possível enviar a ocorrência. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                        }
                        else
                        {
                            await app.MainPage.DisplayAlert("Sucesso", "Ocorrência enviada com sucesso.", "OK");
                            await app.GoBack();
                        }
                    }
                }
            }
            else
            {
                await app.MainPage.DisplayAlert("Erro", "Você deve preencher o tipo e o assunto da mensagem!", "OK");
            }
        }

        private async void GoBack(object sender, EventArgs e)
        {
            await app.GoBack();
        }

        private void Anonymous_Toggled(object sender, ToggledEventArgs e)
        {
            EntryName.IsVisible = !e.Value;
            EntryEmail.IsVisible = !e.Value;
            EntryTelephone.IsVisible = !e.Value;
            LayoutResponse.IsVisible = !e.Value;
        }

        protected override bool OnBackButtonPressed()
        {
            app.GoBack();
            return true;
        }
    }
}
