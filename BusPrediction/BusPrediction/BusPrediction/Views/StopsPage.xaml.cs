﻿using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BusPrediction.Models;
using BusPrediction.ViewModels;

namespace BusPrediction.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StopsPage : ContentPage
    {
        private App app;
        private StopsViewModel stopsViewModel;

        public StopsPage(App app, List<Stop> stops)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            this.app = app;
            stopsViewModel = new StopsViewModel(app, stops);
            BindingContext = stopsViewModel;
            ListViewStops.ItemSelected += ListViewStops_ItemSelected;
        }

        private async void ListViewStops_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedStop = e.SelectedItem as Stop;
            if (selectedStop != null)
            {
                ListViewStops.SelectedItem = null;
                await app.GoToPage(new BusesPage(app, selectedStop));
            }
        }

        private void SearchBarStop_TextChanged(object sender, TextChangedEventArgs e)
        {
            stopsViewModel.FilterStops(e.NewTextValue);
        }

        protected override bool OnBackButtonPressed()
        {
            app.GoBack();
            return true;
        }
    }
}
