﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BusPrediction.Models;
using BusPrediction.ViewModels;

namespace BusPrediction.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BusesPage : ContentPage
    {
        private App app;
        private BusesViewModel busesViewModel;
        private Stop stop;

        public BusesPage(App app, Stop stop)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            this.app = app;
            this.stop = stop;
            busesViewModel = new BusesViewModel(app, stop);
            BindingContext = busesViewModel;
            ListViewBuses.ItemSelected += ListViewBuses_ItemSelected;
        }

        private async void ListViewBuses_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedBus = e.SelectedItem as Bus;
            if (selectedBus != null)
            {
                ListViewBuses.SelectedItem = null;
                await busesViewModel.OpenMap(selectedBus);
            }
        }

        protected override bool OnBackButtonPressed()
        {
            app.GoBack();
            return true;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await busesViewModel.UpdateBuses();
        }
    }
}
