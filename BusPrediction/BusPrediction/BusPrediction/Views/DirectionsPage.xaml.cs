﻿using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BusPrediction.Models;
using BusPrediction.ViewModels;

namespace BusPrediction.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DirectionsPage : ContentPage
    {
        private App app;
        private DirectionsViewModel directionsViewModel;

        public DirectionsPage(App app, List<Direction> directions)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            this.app = app;
            directionsViewModel = new DirectionsViewModel(app, directions);
            BindingContext = directionsViewModel;
        }
    }
}
