﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BusPrediction.ViewModels;

namespace BusPrediction.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMenuPage : ContentPage
    {
        private App app;
        private MainMenuViewModel mainMenuViewModel;

        public MainMenuPage(App app)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            this.app = app;
            mainMenuViewModel = new MainMenuViewModel(app);
            BindingContext = mainMenuViewModel;
        }
    }
}
