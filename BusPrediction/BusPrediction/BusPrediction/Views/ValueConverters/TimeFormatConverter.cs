﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace BusPrediction.Views.ValueConverters
{
    public class TimeFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int minutes = (int)value;
            string converted;
            if (minutes < 1)
                converted = "No local";
            else if (minutes < 3)
                converted = "Aproximando";
            else
                converted = string.Format("{0:00}:{1:00}", minutes / 60, minutes % 60);
            return converted;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
