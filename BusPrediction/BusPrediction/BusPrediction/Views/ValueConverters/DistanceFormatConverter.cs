﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace BusPrediction.Views.ValueConverters
{
    public class DistanceFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int distance = (int)value;
            if (distance > 1000)
            {
                return string.Format("{0:0.00} km", (float)distance / 1000f);
            }
            else
            {
                return string.Format("{0} m", distance);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
