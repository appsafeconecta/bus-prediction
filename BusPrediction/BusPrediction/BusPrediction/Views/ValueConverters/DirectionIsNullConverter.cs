﻿using System;
using System.Globalization;
using Xamarin.Forms;
using BusPrediction.Models;

namespace BusPrediction.Views.ValueConverters
{
    public class DirectionIsNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Direction direction = (Direction)value;
            return direction != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
