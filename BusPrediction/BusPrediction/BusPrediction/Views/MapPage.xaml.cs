﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;
using BusPrediction.Models;
using BusPrediction.Views.CustomRenderers;
using BusPrediction.Models.Transoft;
using BusPrediction.Models.Location;

namespace BusPrediction.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        private App app;
        private int busId;
        private bool stopTimer;
        private List<Stop> stops;
        private Position lastBusPosition;
        private double lastBusSpeed;
        private int initialNearestId;
        private int nearestId;
        private RoutePoints routePoints;
        private string busAddress;

        private double simStep = 1 / 1.0;
        private const double testSpeed = 4.0;
        private double currStep = -1;

        public MapPage(App app, Bus bus, List<Stop> stops, RoutePoints routePoints)
        {
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            this.app = app;
            this.busId = bus.IdDispositivo;
            this.stops = stops;
            this.routePoints = routePoints;

            var locationPermissionVerifier = DependencyService.Get<ILocationPermissionVerifier>();
            map.IsShowingUser = locationPermissionVerifier.IsPermissionGranted();

            CloseImage.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    await app.CloseModal();
                }),
                NumberOfTapsRequired = 1
            });

            lastBusPosition = new Position(bus.LatitudeCarro, bus.LongitudeCarro);
            busAddress = bus.Endereco;
            System.Diagnostics.Debug.WriteLine(lastBusPosition.Latitude + ", " + lastBusPosition.Longitude);
            CustomPin busPin = new CustomPin
            {
                Pin = new Pin
                {
                    Label = bus.IdentificadorVeiculo,
                    Position = lastBusPosition,
                    Address = busAddress
                },
                Representation = PinRepresentation.Bus
            };
            map.BusPins = new List<CustomPin> { busPin };
            map.Pins.Add(busPin.Pin);

            map.StopPins = new List<CustomPin>();
            foreach (var stop in stops)
            {
                CustomPin stopPin = new CustomPin
                {
                    Pin = new Pin
                    {
                        Label = stop.Descricao,
                        Position = new Position(stop.LatitudeCkpt, stop.LongitudeCkpt),
                        Address = stop.Endereco
                    },
                    Representation = PinRepresentation.Stop
                };
                map.StopPins.Add(stopPin);
                map.Pins.Add(stopPin.Pin);
            }

            //map.RouteCoordinates.Add(new Position(busPin.Pin.Position.Latitude, busPin.Pin.Position.Longitude));
            initialNearestId = routePoints.GetNearestIdFrom(bus.LatitudeCarro, bus.LongitudeCarro);
            nearestId = initialNearestId;
            lastBusSpeed = nearestId <= 1 ? 0.0 : testSpeed;
            foreach (var routePoint in routePoints)
            {
                if (routePoint.Id > nearestId)
                    map.RouteCoordinates.Add(new Position(routePoint.Latitude, routePoint.Longitude));
            }

            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(bus.LatitudeCarro, bus.LongitudeCarro), Distance.FromMeters(750)));

            UpdateBus();
            //TrackBus();
        }

        private void UpdateBus()
        {
            Device.StartTimer(TimeSpan.FromSeconds(simStep), () =>
            {
                if (stopTimer)
                    return false;

                Device.BeginInvokeOnMainThread(() =>
                {
                    int currRoutePointId = (int)Math.Floor(currStep);
                    Position currPos;
                    Position nextPos;
                    if (map.RouteCoordinates.Count > currRoutePointId && currRoutePointId >= 0 && currRoutePointId > (nearestId - initialNearestId - 1))
                        currPos = map.RouteCoordinates[currRoutePointId];
                    else
                        currPos = lastBusPosition;
                    if (map.RouteCoordinates.Count > currRoutePointId + 1 && currRoutePointId + 1 >= 0 && currRoutePointId + 1 > (nearestId - initialNearestId - 1))
                        nextPos = map.RouteCoordinates[currRoutePointId + 1];
                    else
                        nextPos = lastBusPosition;
                    double latDiff = nextPos.Latitude - currPos.Latitude;
                    double lngDiff = nextPos.Longitude - currPos.Longitude;
                    double interpolation = currStep - currRoutePointId;
                    Position simPos = new Position(currPos.Latitude + latDiff * interpolation, currPos.Longitude + lngDiff * interpolation);
                    CustomPin busPin = new CustomPin
                    {
                        Pin = new Pin
                        {
                            Label = map.BusPins[0].Pin.Label,
                            Position = simPos,
                            Address = busAddress
                        },
                        Representation = PinRepresentation.Bus
                    };
                    map.BusPins = new List<CustomPin> { busPin };

                    double distance = Formulas.GetKilometersBetween(nextPos.Latitude, nextPos.Longitude, currPos.Latitude, currPos.Longitude);
                    double speed = lastBusSpeed / 3600.0;
                    if (distance > 0)
                        currStep += (speed * simStep) / distance;
                });

                return true;
            });

            Device.StartTimer(TimeSpan.FromSeconds(30), () =>
            {
                System.Diagnostics.Debug.WriteLine("Update Real Bus");

                if (stopTimer)
                    return false;

                Task.Run(async () =>
                {
                    await app.UpdateRoutes(true);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Bus trackingBus = GetTrackingBus();
                        if (trackingBus != null)
                        {
                            lastBusPosition = new Position(trackingBus.LatitudeCarro, trackingBus.LongitudeCarro);
                            busAddress = trackingBus.Endereco;
                            nearestId = routePoints.GetNearestIdFrom(trackingBus.LatitudeCarro, trackingBus.LongitudeCarro);
                            lastBusSpeed = nearestId <= 1 ? 0.0 : testSpeed;
                            currStep = nearestId - initialNearestId - 1;
                            System.Diagnostics.Debug.WriteLine(lastBusPosition.Latitude + ", " + lastBusPosition.Longitude);
                        }
                    });
                });

                return true;
            });
        }

        private void TrackBus()
        {
            Device.StartTimer(TimeSpan.FromSeconds(30), () =>
            {
                if (stopTimer)
                    return false;

                Task.Run(async () =>
                {
                    await app.UpdateRoutes(true);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Bus trackingBus = GetTrackingBus();
                        if (trackingBus != null)
                        {
                            Pin busPin = new Pin
                            {
                                Label = trackingBus.IdentificadorVeiculo,
                                Position = new Position(trackingBus.LatitudeCarro, trackingBus.LongitudeCarro),
                                Address = trackingBus.Endereco
                            };
                            map.Pins.Clear();
                            map.Pins.Add(busPin);
                            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(trackingBus.LatitudeCarro, trackingBus.LongitudeCarro), new Distance(750)));
                        }
                    });

                });

                return true;
            });
        }

        private Bus GetTrackingBus()
        {
            foreach (var route in app.Routes)
                foreach (var direction in route.LsRoteiro)
                    foreach (var stop in direction.LsPontoInteresse)
                        foreach (var bus in stop.LsVeiculo)
                            if (bus.IdDispositivo == busId)
                                return bus;
            return null;
        }

        protected override void OnDisappearing()
        {
            stopTimer = true;
            base.OnDisappearing();
        }
    }
}
