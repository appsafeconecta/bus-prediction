﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusPrediction.Models;

namespace BusPrediction.Services
{
    public class RoutesService : IRoutesService
    {
        public async Task<List<Route>> GetRoutes(string url)
        {
            List<Route> routes;
            using (var client = new System.Net.Http.HttpClient())
            {
                var json = await client.GetStringAsync(url);
                if(json.StartsWith("<!DOCTYPE html>"))
                {
                    string authUrl = url.Remove(url.LastIndexOf('/')) + "/IndexMobile";
                    await client.GetStringAsync(authUrl);
                    json = await client.GetStringAsync(url);
                }

                routes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Route>>(json);

                foreach(var route in routes)
                {
                    foreach(var direction in route.LsRoteiro)
                    {
                        foreach(var stop in direction.LsPontoInteresse)
                        {
                            stop.Direction = direction;
                        }
                        direction.Route = route;
                    }
                }
            }
            return routes;
        }
    }
}
