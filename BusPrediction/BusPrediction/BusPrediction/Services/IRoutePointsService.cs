﻿using BusPrediction.Models.Transoft;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusPrediction.Services
{
    public interface IRoutePointsService
    {
        Task<List<RoutePoint>> GetRoutePoints(int routeId);
    }
}
