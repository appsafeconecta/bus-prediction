﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BusPrediction.Models.Transoft;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace BusPrediction.Services
{
    public class RoutePointsService : IRoutePointsService
    {
        public async Task<List<RoutePoint>> GetRoutePoints(int routeId)
        {
            /*
            List<RoutePoint> routePoints;
            using (var client = new System.Net.Http.HttpClient())
            {
                string authUrl = "http://scgen.com.br/WebServices/Binder/Transoft/AutenticarUsuario?usuario=wsrafael.pontes&senha=r@f@3lp0nt3s";
                var authJson = await client.GetStringAsync(authUrl);
                Authentication auth = Newtonsoft.Json.JsonConvert.DeserializeObject<Authentication>(authJson);
                if (!auth.RetornoOK)
                    return null;
                string routePointsUrl =
                    "http://scgen.com.br/WebServices/Binder/Transoft/ObterPontosRoteiroViagemLinha?guidIdentificacao=" +
                    auth.IdentificacaoLogin +
                    "&idEmpresa=1326&idLinha=" + routeId;
                var routePointsJson = await client.GetStringAsync(routePointsUrl);
                routePoints = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RoutePoint>>(routePointsJson);

                JArray a = JArray.Parse(routePointsJson);
                int i = 0;
                foreach (JObject o in a.Children<JObject>())
                {
                    int id = (int)o["$id"];
                    routePoints[i].Id = id;
                    i++;
                }

            }
            return routePoints.OrderBy(o => o.Id).ToList();
            */
            return new List<RoutePoint>();
        }
    }
}
