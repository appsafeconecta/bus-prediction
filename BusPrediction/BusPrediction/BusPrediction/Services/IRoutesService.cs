﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusPrediction.Models;

namespace BusPrediction.Services
{
    public interface IRoutesService
    {
        Task<List<Route>> GetRoutes(string url);
    }
}
