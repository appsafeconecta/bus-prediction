﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using BusPrediction.Models;
using BusPrediction.Services;
using BusPrediction.Views;
using System.Collections.Generic;
using BusPrediction.Models.Transoft;

namespace BusPrediction
{
    public partial class App : Application
    {
        public RoutesUpdate Routes { get; set; }
        public List<RoutePoints> RoutePointsList { get; set; }

        public App()
        {
            InitializeComponent();

            Routes = new RoutesUpdate();
            RoutePointsList = new List<RoutePoints>();
        }

        public async Task GoToPage(ContentPage newPage)
        {
            await MainPage.Navigation.PushAsync(newPage, false);
        }

        public async Task OpenModal(ContentPage newPage)
        {
            await MainPage.Navigation.PushModalAsync(newPage, true);
        }

        public async Task GoBack()
        {
            await MainPage.Navigation.PopAsync(false);
        }

        public async Task CloseModal()
        {
            await MainPage.Navigation.PopModalAsync(true);
        }

        public async Task GoHome()
        {
            await MainPage.Navigation.PopToRootAsync(false);
        }

        public async Task<bool> UpdateRoutes(bool forceUpdate = false)
        {
            if (DateTime.UtcNow > Routes.LastUpdate + new TimeSpan(0, 0, 30) || forceUpdate)
            {
                Exception error = null;
                try
                {
                    var routesService = new RoutesService();
                    var items = await routesService.GetRoutes(Constants.Url);
                    Routes.Clear();
                    foreach (var route in items)
                    {
                        Routes.Add(route);
                    }
                    Routes.LastUpdate = DateTime.UtcNow;
                }
                catch (Exception ex)
                {
                    error = ex;
                }
                if (error != null)
                {
                    await MainPage.DisplayAlert("Erro", "Não foi possível retornar as linhas. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                    return false;
                }
            }
            return true;
        }

        public RoutePoints GetRoutePoints(int routeId)
        {
            foreach (var routePoints in RoutePointsList)
                if (routePoints.LinhaCodigo == routeId)
                    return routePoints;
            return null;
        }

        protected async override void OnStart()
        {
            MainPage = new SplashPage();
            await UpdateRoutes();
            MainPage = new NavigationPage(new MainMenuPage(this));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
