﻿using System.Collections.Generic;

namespace BusPrediction.Models
{
    public class Stop
    {
        public int IdPontoInteresse { get; set; }
        public string Descricao { get; set; }
        public string Endereco { get; set; }
        public List<Bus> LsVeiculo { get; set; }
        public float LatitudeCkpt { get; set; }
        public float LongitudeCkpt { get; set; }

        public Direction Direction { get; set; }
        public double DistanceFromUser { get; set; }
    }
}
