﻿
namespace BusPrediction.Models.Location
{
    public interface ILocationPermissionVerifier
    {
        bool IsPermissionGranted();
    }
}
