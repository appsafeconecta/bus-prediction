﻿
namespace BusPrediction.Models.Location
{
    public struct GeographicLocation
    {
        public GeographicLocation(double latitude, double longitude) : this()
        {
            Latitude = latitude % 90;
            Longitude = longitude % 180;
        }

        public double Latitude { get; private set; }
        public double Longitude { get; private set; }
    }
}
