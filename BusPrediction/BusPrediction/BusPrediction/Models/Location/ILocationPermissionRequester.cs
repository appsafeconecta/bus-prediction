﻿using System;

namespace BusPrediction.Models.Location
{
    public interface ILocationPermissionRequester
    {
        event EventHandler<bool> RequestLocationPermissionResult;
        void RequestLocationPermission();
    }
}
