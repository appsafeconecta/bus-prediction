﻿using System;

namespace BusPrediction.Models.Location
{
    public interface ILocationTracker
    {
        event EventHandler<GeographicLocation> LocationChanged;
        void StartTracking();
        void PauseTracking();
    }
}
