﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BusPrediction.Models
{
    public class StopGroup : ObservableCollection<Stop>
    {
        public string Title { get; set; }

        public StopGroup(string title, IEnumerable<Stop> collection)
            : base(collection)
        {
            Title = title;
        }
    }
}
