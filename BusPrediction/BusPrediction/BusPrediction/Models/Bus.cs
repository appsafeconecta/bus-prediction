﻿
namespace BusPrediction.Models
{
    public class Bus
    {
        public int IdDispositivo { get; set; }
        public string IdentificadorVeiculo { get; set; }
        public int TempoRestante { get; set; }
        public bool FlAdaptadoCadeirante { get; set; }
        public bool FlEnvioMensagem { get; set; }
        public string Endereco { get; set; }
        public int Distancia { get; set; }
        public float LatitudeCarro { get; set; }
        public float LongitudeCarro { get; set; }
    }
}
