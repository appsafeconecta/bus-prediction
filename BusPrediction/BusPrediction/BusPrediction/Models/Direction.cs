﻿using System.Collections.Generic;

namespace BusPrediction.Models
{
    public class Direction
    {
        public int TipoRoteiro { get; set; }
        public string Roteiro { get; set; }
        public List<Stop> LsPontoInteresse { get; set; }

        public Route Route { get; set; }
    }
}
