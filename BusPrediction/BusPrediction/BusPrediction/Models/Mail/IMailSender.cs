﻿using System;
using System.Threading.Tasks;

namespace BusPrediction.Models.Mail
{
    public interface IMailSender
    {
        Task Send(string name, string email, string type, string subject, string message, string onibus, string linha, string telephone, bool wantsResponse, DateTime date);
    }
}
