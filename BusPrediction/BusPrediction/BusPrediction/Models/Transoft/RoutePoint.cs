﻿using Newtonsoft.Json;
using System.Runtime.Serialization;
using Xamarin.Forms.Maps;

namespace BusPrediction.Models.Transoft
{
    public class RoutePoint
    {
        public int Id { get; set; }
        public bool FlSegunda { get; set; }
        public bool FlTerca { get; set; }
        public bool FlQuarta { get; set; }
        public bool FlQuinta { get; set; }
        public bool FlSexta { get; set; }
        public bool FlSabado { get; set; }
        public bool FlDomingo { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        public Position ToPosition()
        {
            return new Position(Latitude, Longitude);
        }
    }
}
