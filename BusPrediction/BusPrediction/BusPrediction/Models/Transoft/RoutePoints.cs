﻿
using BusPrediction.Models.Location;
using System;
using System.Collections.Generic;

namespace BusPrediction.Models.Transoft
{
    public class RoutePoints : List<RoutePoint>
    {
        public int LinhaCodigo { get; set; }

        public RoutePoints(int routeId)
        {
            LinhaCodigo = routeId;
        }

        public int GetNearestIdFrom(float latitude, float longitude)
        {
            int nearestId = 0;
            double shortestDistance = Double.MaxValue;
            foreach (var routePoint in this)
            {
                double distance = Formulas.GetKilometersBetween(routePoint.Latitude, routePoint.Longitude, latitude, longitude);
                if (distance < shortestDistance)
                {
                    shortestDistance = distance;
                    nearestId = routePoint.Id;
                }
            }
            return nearestId;
        }
    }
}
