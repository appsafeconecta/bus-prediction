﻿using System.Collections.Generic;

namespace BusPrediction.Models
{
    public class Route
    {
        public int LinhaCodigo { get; set; }
        public string LinhaDescricao { get; set; }
        public string CdAgrupamento { get; set; }
        public List<Direction> LsRoteiro { get; set; }
    }
}
