﻿using System;
using System.Collections.Generic;

namespace BusPrediction.Models
{
    public class RoutesUpdate : List<Route>
    {
        public DateTime LastUpdate { get; set; }

        public RoutesUpdate()
        {
            LastUpdate = DateTime.MinValue;
        }
    }
}
