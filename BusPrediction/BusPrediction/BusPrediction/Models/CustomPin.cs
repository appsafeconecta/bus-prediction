﻿using Xamarin.Forms.Maps;

namespace BusPrediction.Models
{
    public enum PinRepresentation { Stop, Bus }
    public class CustomPin
    {
        public Pin Pin { get; set; }
        public PinRepresentation Representation { get; set; }
    }
}
