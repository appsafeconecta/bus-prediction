﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;

using Xamarin.Forms;
using BusPrediction.Models.Location;

[assembly: Dependency(typeof(BusPrediction.Droid.LocationPermissionRequester))]
namespace BusPrediction.Droid
{
    public class LocationPermissionRequester : ILocationPermissionRequester
    {
        readonly string[] PermissionsLocation =
        {
            Manifest.Permission.AccessCoarseLocation,
            Manifest.Permission.AccessFineLocation
        };

        public const int RequestLocationId = 0;

        public event EventHandler<bool> RequestLocationPermissionResult;

        public LocationPermissionRequester()
        {
            Toolkit.Activity.LocationPermissionRequester = this;
        }

        public void RequestLocationPermission()
        {
            ActivityCompat.RequestPermissions(Toolkit.Activity, PermissionsLocation, RequestLocationId);
        }

        public void OnRequestLocationPermissionResult(bool result)
        {
            EventHandler<bool> handler = RequestLocationPermissionResult;
            if (handler != null)
            {
                handler(this, result);
            }
        }

    }
}