﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Forms;
using BusPrediction.Models.Mail;
using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;

[assembly: Dependency(typeof(BusPrediction.Droid.MailSender))]
namespace BusPrediction.Droid
{
    public class MailSender : IMailSender
    {
        public async Task Send(string name, string email, string type, string subject, string message, string onibus, string linha, string telephone, bool wantsResponse, DateTime date)
        {
            var mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("Aplicativo SafeConecta Android", "appsafeconecta@gmail.com"));
            mailMessage.To.Add(new MailboxAddress(Constants.ContactName, Constants.ContactEmail));
            mailMessage.Subject = "Ocorrência relatada pelo Aplicativo SafeConecta Android";

            string msgBody = "";
            if (string.IsNullOrWhiteSpace(name) && string.IsNullOrWhiteSpace(email) && string.IsNullOrWhiteSpace(telephone))
            {
                msgBody += "Um usuário anônimo";
            }
            else
            {
                msgBody += "O seguinte usuário: (";
                if (!string.IsNullOrWhiteSpace(name))
                {
                    msgBody += "Nome: " + name;
                }
                if (!string.IsNullOrWhiteSpace(email))
                {
                    if (!string.IsNullOrWhiteSpace(name))
                        msgBody += ", ";
                    msgBody += "E-Mail: " + email;
                }
                if (!string.IsNullOrWhiteSpace(telephone))
                {
                    if (!string.IsNullOrWhiteSpace(name) || !string.IsNullOrWhiteSpace(email))
                        msgBody += ", ";
                    msgBody += "Telefone: " + telephone;
                }
                msgBody += ")";
            }
            msgBody += @" relatou dia " +
                    date.ToString("dd/MM/yyyy") + " às " + date.ToString("HH:mm:ss") + " um(a) " + type +
                    " acerca do(a) " + subject + ".";

            if (!string.IsNullOrWhiteSpace(onibus))
            {
                msgBody += @"

Número Ônibus:
" + onibus;
            }
            if (!string.IsNullOrWhiteSpace(linha))
            {
                msgBody += @"

Linha:
" + linha;
            }
            if (!string.IsNullOrWhiteSpace(message))
            {
                msgBody += @"

Mensagem:
" + message;
            }
            if (wantsResponse)
            {
                msgBody += @"

O usuário deseja retorno da empresa.";
            }

            mailMessage.Body = new TextPart("plain")
            {
                Text = msgBody
            };

            try
            {
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    await client.ConnectAsync("smtp.gmail.com", 587);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync("appsafeconecta", "SafeConectaApp@2017");

                    await client.SendAsync(mailMessage);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}