﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using BusPrediction.Views.CustomRenderers;
using BusPrediction.Droid.CustomRenderers;

[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(AndroidTimePicker))]
namespace BusPrediction.Droid.CustomRenderers
{
    public class AndroidTimePicker : TimePickerRenderer
    {
        private EditText textField { get; set; }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.TimePicker> e)
        {
            base.OnElementChanged(e);

            if (textField == null)
            {
                textField = new EditText(this.Context)
                {
                    Focusable = false,
                    Clickable = false,
                    Tag = this
                };

                textField.Click += TextField_Click;
                SetNativeControl(textField);
            }

            textField.Text = DateTime.Today.Add(Element.Time).ToString(Element.Format);
        }

        private void TextField_Click(object sender, EventArgs e)
        {
            new TimePickerDialog(this.Context, new EventHandler<TimePickerDialog.TimeSetEventArgs>(OnTimeSet), Element.Time.Hours, Element.Time.Minutes, true).Show();
        }

        private void OnTimeSet(object sender, TimePickerDialog.TimeSetEventArgs e)
        {
            Element.Time = new TimeSpan(e.HourOfDay, e.Minute, 0);
            textField.Text = DateTime.Today.Add(Element.Time).ToString(Element.Format);
        }

    }
}