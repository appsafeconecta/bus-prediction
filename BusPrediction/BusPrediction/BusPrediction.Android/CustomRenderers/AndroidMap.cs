﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;

using Xamarin.Forms;
using Xamarin.Forms.Maps.Android;
using Xamarin.Forms.Platform.Android;
using BusPrediction.Views.CustomRenderers;
using BusPrediction.Droid.CustomRenderers;
using Xamarin.Forms.Maps;
using System.ComponentModel;
using BusPrediction.Models;

[assembly: ExportRenderer(typeof(CustomMap), typeof(AndroidMap))]
namespace BusPrediction.Droid.CustomRenderers
{
    public class AndroidMap : MapRenderer, IOnMapReadyCallback
    {
        //private GoogleMap map;
        private bool isDrawn;
        private CustomMap customMap;
        private Dictionary<string, Marker> busMarkers;

        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                // Unsubscribe
            }

            if (e.NewElement != null)
            {
                customMap = (CustomMap)e.NewElement;

                ((MapView)Control).GetMapAsync(this);
            }
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            InvokeOnMapReadyBaseClassHack(googleMap);

            NativeMap = googleMap;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals("VisibleRegion") && !isDrawn)
            {
                NativeMap.Clear();

                foreach (var pin in customMap.StopPins)
                {
                    var markerOptions = new MarkerOptions();
                    markerOptions.SetPosition(new LatLng(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
                    markerOptions.SetTitle(pin.Pin.Label);
                    markerOptions.SetSnippet(pin.Pin.Address);

                    /*
                    if (pin.Representation == PinRepresentation.Stop)
                        markerOptions.SetIcon(Android.Gms.Maps.Model.BitmapDescriptorFactory.FromResource(Resource.Drawable.ic_place_red));
                    */
                    if (pin.Representation == PinRepresentation.Bus)
                        markerOptions.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.ic_directions_bus));

                    NativeMap.AddMarker(markerOptions);
                }
                UpdateBusMarkers();

                if (customMap.RouteCoordinates.Any())
                {
                    var polylineOptions = new PolylineOptions();
                    polylineOptions.InvokeColor(0x66FF0000);

                    foreach (var position in customMap.RouteCoordinates)
                        polylineOptions.Add(new LatLng(position.Latitude, position.Longitude));

                    NativeMap.AddPolyline(polylineOptions);
                }

                isDrawn = true;
            }

            if (e.PropertyName.Equals(CustomMap.BusPinsProperty.PropertyName))
            {
                UpdateBusMarkers();
            }
        }

        private void UpdateBusMarkers()
        {
            if (busMarkers == null)
                busMarkers = new Dictionary<string, Marker>();

            foreach (CustomPin busPin in customMap.BusPins)
            {
                string busId = busPin.Pin.Label;
                if (!busMarkers.ContainsKey(busId))
                {
                    System.Diagnostics.Debug.WriteLine("Create marker " + busId);
                    var markerOptions = new MarkerOptions();
                    markerOptions.SetPosition(new LatLng(busPin.Pin.Position.Latitude, busPin.Pin.Position.Longitude));
                    markerOptions.SetTitle(busPin.Pin.Label);
                    markerOptions.SetSnippet(busPin.Pin.Address);
                    markerOptions.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.ic_directions_bus));
                    markerOptions.Anchor(0.5f, 0.5f);
                    markerOptions.Visible(true);

                    Marker busMarker = NativeMap.AddMarker(markerOptions);
                    busMarkers.Add(busId, busMarker);
                }
                else
                {
                    Marker busMarker = busMarkers[busId];
                    busMarker.Position = new LatLng(busPin.Pin.Position.Latitude, busPin.Pin.Position.Longitude);
                    busMarker.Snippet = busPin.Pin.Address;
                    busMarker.Visible = true;
                }
            }
        }

        private void InvokeOnMapReadyBaseClassHack(GoogleMap googleMap)
        {
            System.Reflection.MethodInfo onMapReadyMethodInfo = null;

            Type baseType = typeof(MapRenderer);
            foreach (var currentMethod in baseType.GetMethods(System.Reflection.BindingFlags.NonPublic |
                                                             System.Reflection.BindingFlags.Instance |
                                                              System.Reflection.BindingFlags.DeclaredOnly))
            {

                if (currentMethod.IsFinal && currentMethod.IsPrivate)
                {
                    if (string.Equals(currentMethod.Name, "OnMapReady", StringComparison.Ordinal))
                    {
                        onMapReadyMethodInfo = currentMethod;

                        break;
                    }

                    if (currentMethod.Name.EndsWith(".OnMapReady", StringComparison.Ordinal))
                    {
                        onMapReadyMethodInfo = currentMethod;

                        break;
                    }
                }
            }

            if (onMapReadyMethodInfo != null)
            {
                onMapReadyMethodInfo.Invoke(this, new[] { googleMap });
            }
        }
    }
}