﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Forms;
using Android.Locations;
using BusPrediction.Models.Location;
using Android;
using Android.Support.V4.Content;
using Android.Content.PM;
using Android.Support.V4.App;

[assembly: Dependency(typeof(BusPrediction.Droid.LocationTracker))]
namespace BusPrediction.Droid
{
    public class LocationTracker : Java.Lang.Object, ILocationTracker, ILocationListener
    {
        Activity activity;
        LocationManager locationManager;
        DateTime lastLocationChanged;

        public event EventHandler<GeographicLocation> LocationChanged;

        public LocationTracker()
        {
            activity = Toolkit.Activity;

            if (activity == null)
                throw new InvalidOperationException("Must call Toolkit.Init before using LocationProvider");

            locationManager = activity.GetSystemService(Context.LocationService) as LocationManager;

        }

        public void StartTracking()
        {
            const string permission = Manifest.Permission.AccessFineLocation;
            if (ContextCompat.CheckSelfPermission(activity, permission) == (int)Permission.Granted)
            {
                IList<string> locationProviders = locationManager.AllProviders;

                foreach (string locationProvider in locationProviders)
                {
                    locationManager.RequestLocationUpdates(locationProvider, 1000, 1, this);
                }
            }
        }

        public void PauseTracking()
        {
            locationManager.RemoveUpdates(this);
        }

        public void OnLocationChanged(Location location)
        {
            EventHandler<GeographicLocation> handler = LocationChanged;

            if (handler != null && DateTime.UtcNow > lastLocationChanged + new TimeSpan(0, 0, 1))
            {
                lastLocationChanged = DateTime.UtcNow;
                handler(this, new GeographicLocation(location.Latitude, location.Longitude));
            }
        }

        public void OnProviderDisabled(string provider)
        {
        }

        public void OnProviderEnabled(string provider)
        {
        }

        public void OnStatusChanged(string provider, [GeneratedEnum]Availability status, Bundle extras)
        {
        }
    }
}