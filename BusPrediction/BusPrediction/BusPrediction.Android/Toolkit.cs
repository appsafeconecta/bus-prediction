﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BusPrediction.Droid
{
    public static class Toolkit
    {
        public static void Init(MainActivity activity, Bundle bundle)
        {
            Activity = activity;
        }

        public static MainActivity Activity { get; private set; }
    }
}